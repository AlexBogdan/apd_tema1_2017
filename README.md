-=-=-=-=-=-=-=-=-=-=-=-=-= Andrei Bogdan Alexandru -=-=-=-=-=-=-=-=-=-=-=-=-=-=
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-= 336CB -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-= Tema 1 - APD -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-

Implementarea temei:
	
	Tema a fost implementata folosind scheletul oferit, modificand doar
structura snake din main.h pentru a putea pastra informatii despre coada si
despre informatiile unui sarpe la un pas anterior (pentru reverse in caz de
coliziune).

								[Algoritm]

	Inainte de rularea iteratiilor de joc, tinem minte si coada fiecarui sarpe,
obtinuta in urma parcurgerii sarpelui pe matrice (coada va avea doar 1 vecin cu
acelasi encoding, acesta fiind cel prin care deja am trecut).
	Rularea unui pas de joc presupune mutarea tuturor serpilor, respectand
directia lor, primita ca input. Vom avea urmatorul algoritm pentru fiecare sarpe

	Mutarea sarpelui:

		-> Pastram coordonatele pentru headul si tailul curent
		-> In momentul in care serpii ajung pe marginea hartii, acestia pot
	trece in marginea opusa
		-> Efectuam mutarea headului si retinem noua pozitie
		-> Pastram valoarea consumata de sarpe prin mutarea headului
		-> Encodingul sarpelui este trecut in noul coord pe world
		-> Cautam urmatorul coord din sarpe de langa tail si mutam tailul acolo,
	punand un 0 in world pe vechea pozitie

	Marcarea coliziunilor:

		-> In cazul in care un sarpe a atins o valoare diferita de 0, marcam
	cu -1 o eventuala coliziune
		! Marcam oricand un sarpe a atins un alt sarpe (chiar daca threadul a
	mutat un sarpe mai rapid si a atins un tail al altui sarpe)

	Verificarea state-ului obtinut (functia -> int check_collision):

		-> Verificam (pentru fiecare snake) daca valoarea pe harta a headului
	sau este 0. Daca gasim astfel de pozitii le marcam cu encodingul snake-ului,
	ele fiind "coliziuni false" aparute in momentul in care un sarpele X a lovit
	coada altui sarpe Y, iar sarpele Y a fost mutat ulterior, astfel Y nu putea
	fi lovit de X, el fiind doar mutat mai greu de un thread mai lent.
		-> Dupa verificarea tuturor coliziunilor false, verificam daca exista,
	oriunde pe harta, valoarea -1. Daca o intalnim, s-a produs o coliziune si
	semnalam acest lucru prin return.

	Reverse (in cazul in care nu avem coliziuni raportate de pasul anterior):
		-> In cazul in care am avut o coliziune, refacem state-ul anterior al
	jocului (harta + informatii din snakes), folosind pozitia anterioara salvata
	in snake.
		-> Head-ul pune in pozitia actuala valoarea consumata (daca nu este -1)
	si se intoarce la vechea pozitie
		-> Tail se intoarce la vechea pozitie si marcheaza cu snake encoding

								[Timp de rulare]

	Algoritmul a fost testat pe cele 3 teste si produce rezultatul dorit.
	Timpii obtinuti pe testul big_input (testand pe fep.grid.pub.ro) sunt:

		-> (serial) 1 core = 3.373293040s
		
		-> 2 cores = 2.343513777s
		-> 4 cores = 1.940243090s
		-> 6 cores = 1.481712777s
		-> 8 cores = 1.426919756s

	Paralelizarea algoritmului descris mai sus a constat (in principal) in
mutarea serpilor la fiecare pas, fiecare thread mutand o parte din serpi.
	Se poate observa scalabilitatea in functie de numarul de core-uri, dar si
faptul ca de la un anumit numar de core-uri nu mai reusim sa obtinem rezultate
cu mult imbunatatite.