echo "1 core = ";
export OMP_NUM_THREADS=1;
./parallel_snake teste/big_input output 20000;

echo "2 cores =";
export OMP_NUM_THREADS=2;
./parallel_snake teste/big_input output 20000;

echo "4 cores =";
export OMP_NUM_THREADS=4;
./parallel_snake teste/big_input output 20000;

echo "6 cores =";
export OMP_NUM_THREADS=6;
./parallel_snake teste/big_input output 20000;

echo "8 cores =";
export OMP_NUM_THREADS=8;
./parallel_snake teste/big_input output 20000;