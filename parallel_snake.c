#include "main.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

struct coord create_point(int x, int y) {
	struct coord p;
	p.line = x;
	p.col = y;

	return p;
}

void copy_point(struct coord* dest, struct coord* source) {
	dest->line = source->line;
	dest->col = source->col;
}

int point_equal(struct coord p1, struct coord p2) {
	return (p1.line == p2.line) && (p1.col == p2.col);
}

int point_in_limits(struct coord p, int n, int m) {
	return (0 <= p.line) && (p.line < n) && (0 <= p.col) && (p.col < m);
}

void move_snake (struct snake* snake, int*** map, int n, int m) {
	struct coord current;
	current.line = snake->head.line;
	current.col = snake->head.col;

	//Update snake's head position
	switch (snake->direction) {
		case 'N':
			if (snake->head.line > 0) {
				snake->head.line = snake->head.line -1;
			} else {
				snake->head.line = n-1;
			}
			break;
		case 'S':
			if (snake->head.line < n-1) {
				snake->head.line = snake->head.line +1;
			} else {
				snake->head.line = 0;
			}
			break;
		case 'E':
			if (snake->head.col < m-1) {
				snake->head.col = snake->head.col +1;
			} else {
				snake->head.col = 0;
			}
			break;
		case 'V':
			if (snake->head.col > 0) {
				snake->head.col = snake->head.col -1;
			} else {
				snake->head.col = m-1;
			}
			break;
	}

	//printf("%d\n", (*map)[snake->head.line][snake->head.col]);

	//Mark snake on the new location
	snake->consumed_value = (*map)[snake->head.line][snake->head.col];
	if ((*map)[snake->head.line][snake->head.col] == 0) {
		(*map)[snake->head.line][snake->head.col] = snake->encoding;
	} else {
		(*map)[snake->head.line][snake->head.col] = -1; // collision
	}
	
	//printf("%d\n", (*map)[snake->head.line][snake->head.col]);


	/*   Update tail   */

	//Unmark tail
	(*map)[snake->tail.line][snake->tail.col] = 0;

	// Searching all neighbours
	if (snake->tail.line > 0 && (*map)[snake->tail.line -1][snake->tail.col] == snake->encoding) {
		snake->tail.line--;
		return ;
	}
	if (snake->tail.col < m-1 && (*map)[snake->tail.line][snake->tail.col +1] == snake->encoding) {
		snake->tail.col++;
		return ;
	}
	if (snake->tail.line < n-1 && (*map)[snake->tail.line +1][snake->tail.col] == snake->encoding) {
		snake->tail.line++;
		return ;
	}
	if (snake->tail.col > 0 && (*map)[snake->tail.line][snake->tail.col -1] == snake->encoding) {
		snake->tail.col--;
		return ;
	}

	// We have to search beyond our limits
	if (snake->tail.line == 0) {
		// Top-Left corner
		if (snake->tail.col == 0) {
			if ((*map)[0][m-1] == snake->encoding) {
				snake->tail.line = 0;
				snake->tail.col = m -1;
				return ;
			}
			if ((*map)[n-1][0] == snake->encoding) {
				snake->tail.line = n-1;
				snake->tail.col = 0;
				return ;
			}
		}
		// Top-Right corner
		if (snake->tail.col == m-1) {
			if ((*map)[0][0] == snake->encoding) {
				snake->tail.line = 0;
				snake->tail.col = 0;
				return ;
			}
			if ((*map)[n-1][m-1] == snake->encoding) {
				snake->tail.line = n-1;
				snake->tail.col = m-1;
				return ;
			}
		}
		// Anywhere on top line
		snake->tail.line = n-1;
		return ;
	}

	if (snake->tail.line == n-1) {
		// Bottom-Left corner
		if (snake->tail.col == 0) {
			if ((*map)[0][0] == snake->encoding) {
				snake->tail.line = 0;
				snake->tail.col = 0;
				return ;
			}
			if ((*map)[n-1][m-1] == snake->encoding) {
				snake->tail.line = n-1;
				snake->tail.col = m-1;
				return ;
			}
		}
		// Bottom-Right corner
		if (snake->tail.col == m-1) {
			if ((*map)[n-1][0] == snake->encoding) {
				snake->tail.line = n-1;
				snake->tail.col = 0;
				return ;
			}
			if ((*map)[0][m-1] == snake->encoding) {
				snake->tail.line = 0;
				snake->tail.col = m-1;
				return ;
			}
		}
		// Anywhere on bottom line
		snake->tail.line = 0;
		return ;
	}

	//Anywhere on left column
	if (snake->tail.col == 0) {
		snake->tail.col = m-1;
		return ;
	}
	// Anywhere on right column
	if (snake->tail.col == m-1) {
		snake->tail.col = 0;
		return ;
	}
}

int check_collision(int ***world, struct snake *snakes, int num_snakes) {
	for (int s = 0; s < num_snakes; s++) {
		if ((*world)[snakes[s].head.line][snakes[s].head.col] == 0) {
			(*world)[snakes[s].head.line][snakes[s].head.col] = snakes[s].encoding;
		}
	}

	for (int s = 0; s < num_snakes; s++) {
		if ((*world)[snakes[s].head.line][snakes[s].head.col] == -1) {
			// printf("Coliziune in punctul (%d, %d)\n", snakes[s].head.line, snakes[s].head.col);
			return 1;
		}
	}
	return 0;
}

// Reverse to the last step
void reverse_step(int ***world, struct snake *snakes, int num_snakes) {
	for (int s = 0; s < num_snakes; s++) {
		if ((*world)[snakes[s].head.line][snakes[s].head.col] == 0 ||
			snakes[s].consumed_value == -1) {
			(*world)[snakes[s].old_tail.line][snakes[s].old_tail.col] = snakes[s].encoding;
			copy_point(&(snakes[s].head), &(snakes[s].old_head));
			copy_point(&(snakes[s].tail), &(snakes[s].old_tail));
			continue ;
		}

		(*world)[snakes[s].head.line][snakes[s].head.col] = snakes[s].consumed_value;
		(*world)[snakes[s].old_tail.line][snakes[s].old_tail.col] = snakes[s].encoding;
		copy_point(&(snakes[s].head), &(snakes[s].old_head));
		copy_point(&(snakes[s].tail), &(snakes[s].old_tail));
	}
}

void run_simulation(int num_lines, int num_cols, int **world, int num_snakes,
	struct snake *snakes, int step_count, char *file_encoding) 
{
	// TODO: Implement Parallel struct snake simulation using the default (env. OMP_NUM_THREADS) 
	// number of threads.
	//
	// DO NOT include any I/O stuff here, but make sure that world and snakes
	// parameters are updated as required for the final state.
	
	int k , s;
	// Find the tail for every snake
	for (s = 0; s < num_snakes; s++) {
		struct coord old, current;
		old.line = -2;
		old.col = -2;
		current.line = snakes[s].head.line;
		current.col = snakes[s].head.col;

		//Looking for snake's tail by traversing him
		while (1) {
			struct coord north = create_point(current.line -1, current.col);
			struct coord west = create_point(current.line, current.col +1);
			struct coord south = create_point(current.line +1, current.col);
			struct coord east = create_point(current.line, current.col -1);

			if (point_in_limits(north, num_lines, num_cols) && ! point_equal(north, old) &&
				world[north.line][north.col] == snakes[s].encoding) {
				//printf("[Snake %d] Nord\n", snakes[s].encoding);
				copy_point(&old, &current);
				copy_point(&current, &north);
				continue;
			}
			if (point_in_limits(west, num_lines, num_cols) && ! point_equal(west, old) &&
				world[west.line][west.col] == snakes[s].encoding) {
				//printf("[Snake %d] Vest\n", snakes[s].encoding);
				copy_point(&old, &current);
				copy_point(&current, &west);
				continue;
			}
			if (point_in_limits(south, num_lines, num_cols) && ! point_equal(south, old) &&
				world[south.line][south.col] == snakes[s].encoding) {
				//printf("[Snake %d] Sud\n", snakes[s].encoding);
				copy_point(&old, &current);
				copy_point(&current, &south);
				continue;
			}
			if (point_in_limits(east, num_lines, num_cols) && ! point_equal(east, old) &&
				world[east.line][east.col] == snakes[s].encoding) {
				//printf("[Snake %d] Est\n", snakes[s].encoding);
				copy_point(&old, &current);
				copy_point(&current, &east);
				continue;
			}
			break;
		}

		copy_point(&(snakes[s].tail), &current);
		//printf("[Snake %d] Head = (%d, %d) | Tail = (%d, %d)\n", snakes[s].encoding, 
		//		snakes[s].head.line, snakes[s].head.col, snakes[s].tail.line, snakes[s].tail.col);
	}

	int problems = 0;
	k = 0;
	#pragma omp parallel
	{
		while(k < step_count) {

			#pragma omp barrier

			#pragma omp sections nowait
			{
				#pragma omp section
				{
					k++;
				}
			}

			#pragma omp for
			for (s = 0; s < num_snakes; s++) {
				copy_point(&(snakes[s].old_head), &(snakes[s].head));
				copy_point(&(snakes[s].old_tail), &(snakes[s].tail));
			}

			#pragma omp for 
			for (s = 0; s < num_snakes; s++) {
				move_snake ((snakes + s), &world, num_lines, num_cols);
			}

			#pragma omp sections nowait
			{
				#pragma omp section
				{
					if (check_collision(&world, snakes, num_snakes)) {
						problems = 1;
					}
				}
			}

			if (problems == 1) {
				break;
			}
		}
	}
	if (problems == 1) {
		reverse_step(&world, snakes, num_snakes);
	}
	
	return ;
}